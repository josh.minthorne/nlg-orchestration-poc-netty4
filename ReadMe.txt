Project based off of: http://shengwangi.blogspot.com/2015/01/camel-netty-hello-world-simple-example.html?m=1

Camel Project for Spring 
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run
    
From another command line

    telnet localhost 7000
    
 Type something and hit enter to see output on server window.


For more help see the Apache Camel documentation

    http://camel.apache.org/
    

