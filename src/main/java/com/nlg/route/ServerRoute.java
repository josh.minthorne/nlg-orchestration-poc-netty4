package com.nlg.route;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;

public class ServerRoute extends RouteBuilder {

	public ServerRoute() {
		// TODO Auto-generated constructor stub
	}

	public ServerRoute(CamelContext context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void configure() throws Exception {
		from("netty4:tcp://localhost:7000?sync=true&allowDefaultCodec=false&encoder=#stringEncoder&decoder=#stringDecoder")
				.to("bean:echoService");
	}

	public static void main(String[] args) throws Exception {
		new Main().run(args);
	}
}
